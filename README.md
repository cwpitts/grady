# Grady

Batch grader tool for more quickly working with assignments.

## Installation
`pip install -r requirements.txt` should install all of the
requirements.

## Configuration
The configuration file will be generated as you go through the prompts
for `grady init`. You will need an API token generated on the Canvas
instance.

## Usage
`grady course grade` will bring up a text interface that will help you
to choose which course and assignment you wish to grade. It will then
download all student assignments and store them in separate folders.
