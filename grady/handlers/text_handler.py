import logging
from datetime import datetime
from pathlib import Path
from pprint import pformat

import requests
from bs4 import BeautifulSoup
from canvasapi.submission import Submission
from requests import Response
from rich.console import Console
from yarl import URL

from .handler import Handler

log: logging.Logger = logging.getLogger("attachmenthandler")


class TextHandler(Handler):
    def __init__(
        self,
        token: str,
        download_dir: Path,
        console: Console,
        due_date: datetime,
        log_level: int = logging.CRITICAL,
    ):
        super().__init__(token, download_dir, console, due_date, log_level)
        log.setLevel(log_level)

    def download(self, submission: Submission) -> bool:
        soup: BeautifulSoup = BeautifulSoup(f"<body>{submission.body}</body>", "lxml")

        submission_file: Path = self._download_dir / f"submission.txt"

        with open(submission_file, "w") as out_file:
            for element in soup.select("body"):
                out_file.write(f"{element.get_text()}")

        return True
