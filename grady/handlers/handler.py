import logging
from abc import ABC, abstractmethod
from datetime import datetime
from pathlib import Path

from canvasapi.submission import Submission
from git import Commit, Repo, exc
from rich.console import Console
from yarl import URL


class Handler(ABC):
    def __init__(
        self,
        token: str,
        download_dir: Path,
        console: Console,
        due_date: datetime,
        log_level: int = logging.CRITICAL,
    ):
        self._token: str = token
        self._download_dir: Path = download_dir
        self._console: Console = console
        self._due_date: datetime = due_date

    @abstractmethod
    def download(self, submission: Submission) -> bool:
        raise NotImplementedError

    def _clone_and_rewind(self, repo_url: URL) -> Exception:
        try:
            repo: Repo = Repo.clone_from(
                repo_url.human_repr(), self._download_dir / "repo"
            )
        except exc.GitCommandError as err:
            self._console.print("[red]Bad repo URL, was:[/red]\n")
            self._console.print(repo_url)
            return err
        except Exception as err:
            return err

        c: Commit = repo.head.commit
        while c.parents and c.committed_datetime > self._due_date:
            c = c.parents[0]
        repo.git.checkout(c)

        if not c.parents:
            self._console.print(
                f"[red]No commits found before due date for {repo_url}[/red]\n"
            )

        return None
