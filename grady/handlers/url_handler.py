import logging
from datetime import datetime
from pathlib import Path

from canvasapi.submission import Submission
from rich.console import Console
from yarl import URL

from .handler import Handler

log: logging.Logger = logging.getLogger("urlhandler")

class URLHandler(Handler):
    def __init__(
        self,
        token: str,
        download_dir: Path,
        console: Console,
        due_date: datetime,
        log_level: int = logging.CRITICAL,
    ):
        super().__init__(token, download_dir, console, due_date, log_level)
        log.setLevel(log_level)

    def download(self, submission: Submission) -> bool:
        if submission.url is None:
            return False

        err: Exception = self._clone_and_rewind(URL(submission.url))
        if err is not None:
            log.warning("Failed to clone/rewind, got error:\n%s", err)
            return False

        return True
