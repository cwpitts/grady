import logging
from datetime import datetime
from pathlib import Path
from pprint import pformat

import magic
import pypandoc
import requests
from canvasapi.submission import Submission
from requests import Response
from rich.console import Console
from yarl import URL

from .handler import Handler

log: logging.Logger = logging.getLogger("attachmenthandler")


class AttachmentHandler(Handler):
    def __init__(
        self,
        token: str,
        download_dir: Path,
        console: Console,
        due_date: datetime,
        log_level: int = logging.CRITICAL,
    ):
        super().__init__(token, download_dir, console, due_date, log_level)
        log.setLevel(log_level)

    def download(self, submission: Submission) -> bool:
        for attachment in submission.attachments:
            log.debug(f"Downloading attachment:\n{pformat(attachment)}")
            url: URL = URL(attachment["url"]).with_scheme("https")
            resp: Response = requests.get(
                url, headers={"Authorization": f"Bearer {self._token}"},
            )

            if attachment["content-type"] == "text/plain":
                fname: Path = self._download_dir / "submission.txt"
                with open(fname, "w") as out_file:
                    out_file.write(resp.text)

                with open(fname, "r") as in_file:
                    repo_url: URL = URL(in_file.readline().strip())

                    err: Exception = self._clone_and_rewind(repo_url)
                    if err is not None:
                        log.warning("Failed to clone/rewind, got error:\n%s", err)
                        return False
            else:
                file_path: Path = self._download_dir / attachment["filename"]
                with open(file_path, "wb") as out_file:
                    out_file.write(resp.content)

                mime_type: str = magic.from_file(file_path)

                if "pdf" in mime_type.lower():
                    return

                pypandoc.convert_file(
                    file_path, "plain", outputfile=self._download_dir / "submission.txt"
                )

        return True
