#!/usr/bin/env python3

import json
import logging
import os
import subprocess
import sys
import tarfile
import zipfile
from argparse import ArgumentParser, Namespace
from datetime import datetime
from getpass import getpass
from pathlib import Path
from pprint import pformat
from typing import Callable, Dict, List
from urllib.parse import ParseResult, urlparse

import pytest
import requests
from canvasapi import Canvas
from canvasapi.assignment import Assignment
from canvasapi.course import Course
from canvasapi.group import Group, GroupCategory
from canvasapi.submission import Submission
from canvasapi.user import User
from dateutil import parser
from dateutil.tz import tzlocal
from git import Commit, Repo, exc
from requests import Response
from rich.console import Console
from rich.progress import Progress
from rich.prompt import IntPrompt, Prompt
from rich.table import Table
from slugify import slugify

from grady.handlers import AttachmentHandler, Handler, TextHandler, URLHandler

log: logging.Logger = logging.getLogger("grady")
logging.basicConfig()

__version__ = "0.1.0"

os.environ["GRADY"] = "1"


class set_directory:
    """ Temporarily set working directory

    Args:
      path (Path): The path to change to
    """

    def __init__(self, path: Path):
        self._new_path = path
        self._original_path = Path().absolute()

    def __enter__(self):
        os.chdir(self._new_path)

    def __exit__(self, *args, **kwargs):
        os.chdir(self._original_path)


def get_url_input(console: Console, prompt: str) -> str:
    """ Get appropriate URL input
    """
    while True:
        url_input: str = Prompt.ask(prompt)
        if not url_input:
            continue

        url: ParseResult = urlparse(url_input)
        if not url.scheme or not url.netloc:
            console.print(
                "[red]URL must be of the form schema://location, e.g. https://canvas.instructure.com[/red]"
            )
            continue

        return f"{url.scheme}://{url.netloc}{url.path}"


def select_course(canvas: Canvas) -> Course:
    """ Select from available courses

    Args:
      canvas (Canvas): Canvas API interface

    Returns:
      course (Course): Selected course object
    """
    console: Console = Console()
    console.clear()

    course_table: Table = Table("", "Course Code", "Name", "Start Date", "End Date")
    courses = list(canvas.get_courses())
    for idx, course in enumerate(courses):
        if hasattr(course, "start_at_date"):
            start_date: datetime = course.start_at_date
            days_until_start: int = (start_date - datetime.now(tz=tzlocal())).days
        else:
            start_date: datetime = None
            days_until_start: int = None
        if hasattr(course, "end_at_date"):
            end_date: datetime = course.end_at_date
            days_until_end: int = (end_date - datetime.now(tz=tzlocal())).days
        else:
            end_date: datetime = None
            days_until_end: int = None
        if start_date:
            unit_str: str = "day"
            if abs(days_until_start) > 1:
                unit_str += "s"
            start_date_fmt: str = f"{start_date.strftime('%Y-%m-%d')} ({days_until_start} {unit_str})"
        else:
            start_date_fmt: str = "N/A"
        if end_date:
            unit_str: str = "day"
            if abs(days_until_end) > 1:
                unit_str += "s"
            end_date_fmt: str = f"{end_date.strftime('%Y-%m-%d')} ({days_until_end} {unit_str})"
        else:
            end_date_fmt: str = "N/A"

        if end_date and days_until_end < 0:
            continue
        if start_date and days_until_start > 0:
            continue

        course_table.add_row(
            str(idx), course.course_code, course.name, start_date_fmt, end_date_fmt
        )

    console.print("[green]Select class[/green]")
    console.print(course_table)

    class_idx_prompt: str = f"Enter integer index [{0}-{len(courses) - 1}]"
    course_idx_input: int = IntPrompt.ask(class_idx_prompt)
    while not 0 <= course_idx_input < len(courses):
        console.print(f"[red]Must be in range {0}-{len(courses) - 1}[/red]")
        course_idx_input = IntPrompt.ask(class_idx_prompt)

    course: Course = courses[course_idx_input]
    return course


def select_assignment(
    course: Course,
    course_config: Dict[str, str],
    assignment_filter: Callable = lambda x: True,
) -> Assignment:
    """ Select from available assignments

    Args:
      course (Course): Canvas Course object
      course_config (Dict[str, str]): Course configuration
      assignment_filter (Callable): Filter function for assignments

    Returns:
      assignment (Assignment): Selected assignment object
    """
    console: Console = Console()
    console.clear()

    assignment_table: Table = Table("", "Assignment", "To Grade", "Test URL")
    assignments: List = list(course.get_assignments())
    for idx, assignment in enumerate(assignments):
        if not assignment_filter(assignment):
            continue
        test_url: str = course_config.get(assignment.id, "No test")
        assignment_table.add_row(
            str(idx), assignment.name, str(assignment.needs_grading_count), test_url
        )
    console.print(assignment_table)

    console.print(f"[green]Select assignment ({0}-{len(assignments) - 1})[/green]")

    assignment_idx_prompt: str = f"Enter integer index [{0}-{len(assignments) - 1}]"
    assignment_idx_input: int = IntPrompt.ask(assignment_idx_prompt)
    while not 0 <= assignment_idx_input < len(assignments):
        console.print(f"[red]Must be in range {0}-{len(assignments) - 1}[/red]")
        assignment_idx_input = IntPrompt.ask(assignment_idx_prompt)

    assignment: Assignment = assignments[assignment_idx_input]
    return assignment


def configure_course(config: Dict[str, str], config_dir: Path):
    """ Add a course configuration

    Args:
      config (Dict[str, str]): Configuration file
      config_dir (Path): Directory where configuration files are stored
    """
    console: Console = Console()
    console.clear()
    console.print(f"[green]Connecting to Canvas instance at {config['url']}[/green]")
    canvas = Canvas(config["url"], config["token"])

    course: Course = select_course(canvas)

    console.clear()
    console.print(
        f"[green]Configuring {course.course_code}: {course.name} ({course.id})[/green]"
    )

    course_config: Dict[str, str] = load_course_config(course.id, config_dir)

    add_test: bool = True
    while add_test:
        assignment = select_assignment(course, course_config)
        url: str = get_url_input(console, "URL for tests")
        course_config[assignment.id] = url
        add_test = Prompt.ask("Continue?", choices=["yes", "no"]) == "yes"

    log.debug(f"Writing config to file:\n{course_config}")
    with open(config_dir / f"{course.id}.json", "w") as out_file:
        json.dump(course_config, out_file, indent=2)


def grade_assignment(config: Dict[str, str], config_dir: Path):
    """ Grade an assignment

    Args:
      config (Dict[str, str]): Program configuration
      config_dir (Path): Path to configuration directory
    """
    console = Console()
    console.clear()

    canvas: Canvas = Canvas(config["url"], config["token"])
    course: Course = select_course(canvas)
    course_config: Dict[str, str] = load_course_config(course.id, config_dir)
    assignment: Assignment = select_assignment(
        course, course_config, assignment_filter=lambda x: x.needs_grading_count > 0
    )
    due_date: datetime = parser.parse(assignment.due_at)

    console.clear()
    console.print(f"[green]Grading assignment {assignment.name}[/green]")

    grading_dir: Path = (Path("grading") / slugify(assignment.name)).resolve()
    if not grading_dir.exists():
        grading_dir.mkdir(parents=True)

    submissions: Dict[int, Submission] = collect_submissions(course, assignment)

    log.debug(f"Submissions:\n{pformat(submissions)}")

    with console.status("[green]Downloading submissions[/green]") as status:
        for entity_id, item in submissions.items():
            log.debug(f"Submission for entity id {entity_id} is:\n{pformat(item)}")
            if item["group"]:
                grading_name = slugify(item["group"].name)
            else:
                grading_name = slugify(item["submitter"].name)
            submission_dir: Path = grading_dir / grading_name

            if not submission_dir.exists():
                submission_dir.mkdir()

            submission: Submission = item["submission"]
            if not submission:
                console.print(f"[red]No submission for {grading_name}[/red]")
                continue

            handler: Handler
            if submission.submission_type in ("file", "online_upload"):
                handler = AttachmentHandler(
                    config["token"],
                    submission_dir,
                    console,
                    due_date,
                    log.getEffectiveLevel(),
                )
            elif submission.submission_type == "online_url":
                handler = URLHandler(
                    config["token"],
                    submission_dir,
                    console,
                    due_date,
                    log.getEffectiveLevel(),
                )
            elif submission.submission_type == "online_text_entry":
                handler = TextHandler(
                    config["token"],
                    submission_dir,
                    console,
                    due_date,
                    log.getEffectiveLevel(),
                )
            else:
                console.print(
                    f"[red]Unsupported submission_type [{submission.submission_type}] for {grading_name} [/red]"
                )
                continue

            log.debug(f"Downloading submission for {grading_name}")
            if not handler.download(submission):
                console.print(
                    f"[red]Failed to download submission for {grading_name}[/red]"
                )


def grady_init(args: Namespace):
    """ Initialize Grady for an instance

    Args:
      args (Namespace): Program arguments
    """
    console = Console()
    console.clear()
    console.print("[green]Canvas access information[/green]")

    # Get basic access information
    url: str = get_url_input(console, "Canvas instance URL")

    token_or_file: str = Prompt.ask(
        "Enter token or path to token file?", choices=["token", "file"]
    )
    token: str = None
    if token_or_file == "file":
        while not token:
            token_path_str: str = Prompt.ask("Path to token file")
            if not token_path_str:
                continue

            token_path: Path = Path(token_path_str).expanduser().resolve()
            if not token_path.exists():
                console.print(
                    f"[red]Invalid path to token file: {str(token_path)}[/red]"
                )
                continue

            with open(token_path, "r") as in_file:
                token = in_file.read().strip()
    else:
        token = Prompt.ask("Canvas API token", password=True)

    console.print("[green]Connecting to Canvas instance[/green]")
    canvas = Canvas(url, token)
    console.print("[green]Connected successfully[/green]")

    if not args.config_path.parent.exists():
        args.config_path.parent.mkdir(parents=True)

    log.debug("Writing config to JSON file")
    with open(args.config_path, "w") as out_file:
        json.dump({"url": url, "token": token}, out_file, indent=2)


def collect_submissions(course: Course, assignment: Assignment) -> Dict:
    console: Console = Console()

    submissions: Dict[int, Dict[str, str]] = {}
    if assignment.group_category_id:
        group_categories = {
            group_category.id: group_category
            for group_category in course.get_group_categories()
        }
        group_category: GroupCategory = group_categories[assignment.group_category_id]
        log.debug(f"Group category is: {group_category.name}")

        groups: Dict[int, Group] = {
            group.id: group for group in group_category.get_groups()
        }

        with console.status("[green]Collecting group submissions[/green]") as status:
            for submission in assignment.get_submissions():
                submitter: User = course.get_user(submission.user_id)

                group: Group = select_group_by_user(groups, submitter)

                insert_id: int = group.id if group else submitter.id

                submissions[insert_id] = {
                    "submission": submission,
                    "submitter": submitter,
                }

                submissions[insert_id]["group"] = group
    else:
        with console.status("[green]Collecting student submissions[/green]") as status:
            for submission in assignment.get_submissions():
                submitter: User = course.get_user(submission.user_id)
                submissions[submitter.id] = {
                    "submission": submission,
                    "submitter": submitter,
                    "group": None,
                }

    return submissions


def select_group_by_user(groups: Dict[int, Group], user: User) -> Group:
    """ Select group by member user ID

    Args:
      groups (Dict[int, Group]): Group mapping
      user (User): User to search for

    Returns:
      group (Group):
        First group of which this user is a member, None if not found
    """
    for _, group in groups.items():
        for membership in group.get_memberships():
            if membership.user_id == user.id:
                return group

    return None


def load_course_config(course_id: int, config_dir: Path) -> Dict[int, str]:
    """ Load course config

    Args:
      course_id (int): Course ID
      config_dir (Path): Path to configuration directory

    Returns:
      course_config (Dict[int, str]):
        Course configuration if present, empty dict otherwise
    """
    course_config: Dict[str, str] = {}
    course_config_path: Path = config_dir / f"{course_id}.json"
    if course_config_path.exists():
        with open(course_config_path, "r") as in_file:
            course_config: Dict[str, str] = json.load(in_file)

    course_config = {int(k): v for k, v in course_config.items()}

    return course_config


def list_courses(config: Dict[str, str], config_dir: Path):
    """ List all classes

    Args:
      config (Dict[str, str]): Main Grady configuration
      config_dir (Path): Directory where configuration files should be stored
    """
    canvas = Canvas(config["url"], config["token"])

    for course in canvas.get_courses():
        course_config_path: Path = config_dir / f"{course.id}.json"
        has_config: bool = course_config_path.exists()

        print(f"{course.course_code}: {course.name} (configured: {has_config})")


def load_config(config_path: Path) -> Dict[str, str]:
    """ Load configuration file

    Returns:
      Dict[str, str]: Configuraton as dictionary
    """
    if not config_path.exists():
        return None

    with open(config_path, "r") as in_file:
        return json.load(in_file)


def parse_args(arglist: List = None) -> Namespace:
    """ Parse and validate command-line arguments

    Returns:
      args (Namespace): Parsed and validated command-line arguments

    Raises:
      ValueError: When values are not correct for arguments
      ArgumentError: When errors are encountered in parsing
    """
    argp: ArgumentParser = ArgumentParser(prog="grady")
    argp.add_argument(
        "-d", "--debug", help="run in debug mode", action="store_true", default=False
    )
    argp.add_argument(
        "-c",
        "--config-path",
        help="path to config file",
        default=Path.home() / ".grady" / "config.json",
        type=Path,
    )

    main_subcommands = argp.add_subparsers(dest="cmd")

    init: ArgumentParser = main_subcommands.add_parser("init")
    init.add_argument(
        "-f",
        "--force",
        help="force initialization",
        action="store_true",
        default=False,
    )

    course: ArgumentParser = main_subcommands.add_parser("course")
    course.add_argument(
        "course_action",
        help="interact with courses",
        choices=("list", "configure", "grade"),
    )

    return argp.parse_args()


def main(args: Namespace = None):
    """ Main program function

    Args:
      args (Namespace): Program arguments
    """
    if not args:
        args = parse_args()

    if args.debug:
        log.setLevel(logging.DEBUG)

    log.debug(f"Running with args:\n{pformat(vars(args))}")

    config: Dict[str, str] = load_config(args.config_path)

    if not config and args.cmd != "init":
        print("Please run 'grady init'")

    if args.cmd == "init":
        log.debug("Initializing Grady")
        if config and not args.force:
            print("Existing Grady configuration found!")
            print("Remove or run with --force to initialize again")
            return
        config = grady_init(args)
    elif args.cmd == "course":
        if args.course_action == "list":
            list_courses(config, args.config_path.parent)
        elif args.course_action == "configure":
            configure_course(config, args.config_path.parent)
        elif args.course_action == "grade":
            grade_assignment(config, args.config_path.parent)


if __name__ == "__main__":
    _args: Namespace = parse_args()
    main(_args)
