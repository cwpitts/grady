import setuptools

with open("README.md", "r", encoding="utf-8") as in_file:
    long_description = in_file.read()

with open("requirements.txt", "r") as in_file:
    requirements = [line.strip() for line in in_file]

setuptools.setup(
    name="grady",
    version="0.1.0",
    author="Christopher Pitts",
    author_email="cwpitts@protonmail.net",
    description="Grading utility tool",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/cwpitts/grady",
    project_urls={"Bug Tracker": "https://gitlab.com/cwpitts/grady/-/issues",},
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    entry_points={"console_scripts": ["grady=grady.grady:main",],},
    packages=setuptools.find_packages(
        include=[
            "grady",
            "grady.pytest_plugins",
            "grady.test_decorators",
            "grady.handlers",
            "grady._logging",
        ]
    ),
    python_requires=">=3.7",
    install_requires=requirements,
)
